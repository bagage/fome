window.jQuery = window.$ =  require('jquery/dist/jquery.min');
require('bootstrap');
require('leaflet');
require('leaflet-fontawesome-markers');
require('@bagage/leaflet.restoreview');
require('leaflet-hash');
require('leaflet.locatecontrol');

require('bootstrap/dist/css/bootstrap.min.css');
require('bootstrap/dist/css/bootstrap-theme.min.css');
require('font-awesome/css/font-awesome.min.css');
require('leaflet-fontawesome-markers/L.Icon.FontAwesome.css');
require('leaflet/dist/leaflet.css');
require('leaflet.locatecontrol/dist/L.Control.Locate.min.css');

require('./fome.css');
require('./fome.js');