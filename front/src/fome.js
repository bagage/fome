import Remarkable from 'remarkable';

var map;
var api = 'https://api.openeventdatabase.org/event';

// Pour chaque requête prédéfinie on associe le div destination

var what = "sport.orienteering.race";
var preDefinedEvents =[
	{"btn":"btn_orienteering_nextweek", "reqDatas":jQuery.param({"limit": 10000, "what":what, "start": "yesterday", "stop": "nextweek"}), "layer": L.geoJson()},
	{"btn":"btn_orienteering_nextmonth", "reqDatas":jQuery.param({"limit": 10000, "what":what, "start": "yesterday", "stop": "nextmonth"}), "layer": L.geoJson()},
	{"btn":"btn_orienteering_nextyear", "reqDatas":jQuery.param({"limit": 10000, "what":what, "start": "yesterday", "stop": "nextyear"}), "layer": L.geoJson()},
];

function getPredefinedEvent(targetId) {
	for(var i=0;i<preDefinedEvents.length;i++) {
		const item = preDefinedEvents[i];
		if(targetId == item.btn) return item;
	}
	return 0;
}

function init() {
	var osmUrl = 'https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png',
		osmAttrib = '&copy; <a href="http://openstreetmap.org/copyright">Tiles © cquest@Openstreetmap France, data © OpenStreetMap contributors, ODBL</a> contributors&nbsp;|&nbsp;Events : <a href="https://github.com/openeventdatabase/backend/wiki">OpenEvenDB</a>',
		osm = L.tileLayer(osmUrl, {maxZoom: 18, attribution: osmAttrib});
	map = new L.Map('map', {layers: [osm], center: new L.LatLng(46.9886, 2.08740), zoom: 6 });
	var fitBoundsInitially = !map.restoreView();

	new L.Hash(map);

    var secureContext = 'isSecureContext' in window ? isSecureContext : location.protocol === 'https:';
    if (secureContext) {
        L.control.locate({
            icon: 'fa fa-location-arrow',
            iconLoading: 'fa fa-spinner fa-pulse',
        }).addTo(map);
    }

	for (var i = 0; i < preDefinedEvents.length; i++) {
		const e = preDefinedEvents[i];
		const id = e["btn"];
		$("#" + id).click(function(event) {
			clearCount(id);
			clearAllEvents();
			updateEvents(e["reqDatas"], id, false);
		});
	}

	const urlParams = new URLSearchParams(window.location.search);
	const untilParam = urlParams.get('until');
	let idx = 0;
	if (untilParam) {
		if (untilParam === 'week') idx = 0;
		else if (untilParam === 'month') idx = 1;
		else if (untilParam === 'year') idx = 2;
	}

	// refresh next week at start
	updateEvents(preDefinedEvents[idx]["reqDatas"], preDefinedEvents[idx]["btn"], fitBoundsInitially);
}

function updateEvents(reqDatas, targetId, bFitBounds) {
	// console.log(reqDatas, targetId);
	$.ajax({
			url : api,
			data: decodeURIComponent(reqDatas),
			dataType : 'json',
			context: {"targetId":targetId, "fitBounds" : bFitBounds}
		}).done(function(events) {
			var context = this;

			const notCancelled = events.features.filter(it => it.properties.status === 'OK')
			$("#"+ context.targetId + " .count").html( notCancelled.length );
			updateMap(context.targetId, events, context.fitBounds);
		});
}


function eventIcon(status, category) {
	if (status === 'Course annulée') {
		return 'times';
	} else if (status && status.indexOf('Course reportée au') !== -1) {
		return 'flag';
	} else {
		switch (category.toLowerCase()) {
	    	case 'ld':
	    		return 'tree'
	    	case 'md':
	    		return 'leaf'
	    	case 'nuit':
	    		return 'moon-o'
	    	case 'raid':
	    		return 'bicycle'
	    	case 'relais':
	    		return 'users'
	    	case 'score':
	    		return 'calculator'
	    	case 'sprint':
	    		return 'road'
	    	default:
				return 'pagelines';
		}
	}
}

function eventMarkerColor(status, isCn, isAccurate) {
	if (status === 'Course annulée') {
		return '#dd0000';
	} else if (status && status.indexOf('Course reportée au') !== -1) {
		return '#ddd200';
	} else if (isAccurate) {
		return isCn ? '#77a9ce' : '#88a9ce'
	} else {
		return isCn ? '#77ddce' : '#88ddce'
	}
}

function updateMap(targetId, events, fitBounds) {
	// console.log(targetId, events, fitBounds);
	const evt = getPredefinedEvent(targetId);
	evt.layer.clearLayers();
	// console.log(evt.layer);
	evt.layer = L.geoJson(events,{pointToLayer: function(feature, latlng){
		const isCn = (feature.properties.cn === true);
		const isAccurate = (feature.properties.accurate_position === true);
		var marker = L.marker(latlng, {
			icon: L.icon.fontAwesome({
		        iconClasses: `fa fa-${eventIcon(feature.properties.status, feature.properties.category || 'other')}`,
		        markerColor: eventMarkerColor(
		        	feature.properties.status,
		        	isCn,
		        	isAccurate),
		        iconColor: '#FFF'
		    })
		});

		var md = new Remarkable({html: true, breaks: true});
		var content = '';

		var title = '';
		if (isCn) {
			title = '<span class="fa fa-check"></span> '
		}
		if (feature.properties.name) title += feature.properties.name;
		else if (feature.properties.label) title += feature.properties.label;
		else title += feature.properties.what;

		var date = '';
		if(feature.properties.start) {
			date = `Début : ${feature.properties.start}\nFin : ${feature.properties.stop}`;
		}else {
			date = `Date : ${feature.properties.when}`;
		}

		var warning = '';
		if (feature.properties.status === 'Course annulée') {
			warning += "<span class=\"fa fa-warning\"></span> *Course annulée.*<br>"
		} else if (feature.properties.status !== 'OK') {
			warning += "<span class=\"fa fa-warning\"></span> *Course reportée à une autre date.*<br>"
		}

		if (!isAccurate) {
			warning += `<span class=\"fa fa-warning\"></span> Adresse précise de l'évenement inconnue`;
			warning += feature.properties.address ?  ` (${feature.properties.address}).` : '.';
		}

		var observations = feature.properties.observations ? "*" + feature.properties.observations + "*" : "";
		var website = feature.properties.source ? "<a href=\"" + feature.properties.source + "\" target=\"_blank\">Site web</a>" : "";
		var category = feature.properties.category ? "Categorie : " + feature.properties.category : "";
		var popup = md.render(`#### ${title}
${warning}

${date}
Niveau : ${feature.properties.level}
Type : ${feature.properties.speciality}
${category}

<small>ID : ${feature.properties.id}</small>

${observations}

${website}`)
		return marker.addTo(map).bindPopup(popup);

	}}).addTo(map);

	evt.layer.bringToFront();

	if(events.count > 0 && fitBounds == true) {
		// console.log(fitBounds);
		map.fitBounds(evt.layer.getBounds());
	}

}


function clearCount(targetId) {
	$("#"+ targetId + " .count").html('Cliquer pour voir');
}

function clearLayer(targetId) {
	const evt = getPredefinedEvent(targetId);
	evt.layer.clearLayers();
}

function clearAllEvents() {
	for(var i=0;i<preDefinedEvents.length;i++) {
		const item = preDefinedEvents[i];
		// clearCount(item.btn);
		clearLayer(item.btn);
	}
}

init();
