const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: ['url-search-params-polyfill', './src/index.js'],
  plugins: [
  new CleanWebpackPlugin(),
  new HtmlWebpackPlugin({
    favicon: './img/logo32.png',
    template: 'src/fome.html'
  }),
  ],
  devServer: {
   contentBase: './dist',
 },
 output: {
  filename: 'main.js',
  path: path.resolve(__dirname, 'dist')
},
module: {
  rules: [
  {
    test: /\.css$/,
    use: ['style-loader', 'css-loader']
  },
  {
    test: /\.(woff|woff2|eot|ttf|otf|svg|png)$/,
    use: ['file-loader']
  }
  ]
}
};