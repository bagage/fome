#!/usr/bin/env python3

import logging
import coloredlogs
import os
import sys
import argparse

from oedb_twitter import tweet_events

LOG = logging.getLogger(__name__)


def main(args):
    return tweet_events(
        args.until,
        args.dry_run,
        args.consumer_key,
        args.consumer_secret,
        args.access_key,
        args.access_secret,
    )


def setup_logging(debug):
    verbosity = {
        "DEBUG": logging.DEBUG,
        "INFO": logging.INFO,
        "WARNING": logging.WARNING,
        "ERROR": logging.ERROR,
        "CRITICAL": logging.CRITICAL,
    }
    coloredlogs.install(
        level=verbosity[
            (os.environ.get("FOME_VERBOSITY") or "DEBUG" if debug else "INFO").upper()
        ],
        logger=LOG,
        fmt="%(asctime)s %(message)s",
    )


class ArgumentParser(object):
    def __init__(self):
        parser = argparse.ArgumentParser(description="tweet upcoming events")

        parser.add_argument("--debug", help="Activate debug logs", action="store_true")
        parser.add_argument(
            "--dry-run",
            "-n",
            help="Simulate actions without executing them (tweets, etc.)",
            action="store_true",
        )

        parser.add_argument("--consumer-key", help="Twitter consumer key")
        parser.add_argument("--consumer-secret", help="Twitter consumer secret")
        parser.add_argument("--access-key", help="Twitter access key")
        parser.add_argument("--access-secret", help="Twitter access secret")

        parser.add_argument(
            "--until",
            help="Upcoming events maximum date",
            choices=["week", "month", "year"],
            required=True,
        )
        self.args = parser.parse_args(sys.argv[1:])


if __name__ == "__main__":
    args = ArgumentParser().args
    setup_logging(args.debug)
    try:
        main(args)
    except KeyboardInterrupt:
        pass
