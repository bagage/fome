#!/usr/bin/env python3

import logging
import requests
import geojson
import sys
import twitter


LOG = logging.getLogger(__name__)
FOME_CATEGORY = "sport.orienteering.race"


def tweet_events(
    until, dry_run, consumer_key, consumer_secret, access_key, access_secret
):
    LOG.debug(f"Querying events until {until}")

    events = None
    with requests.Session() as s:
        url = "https://api.openeventdatabase.org/event"
        params = {
            "what": FOME_CATEGORY,
            "status": "OK",
            "limit": 10000,
            "start": "yesterday",
            "stop": f"next{until}",
        }
        response = s.get(url, params=params)
        if response.status_code != 200:
            LOG.error("Could not fetch events")
            sys.exit(1)

        events = [
            x
            for x in geojson.loads(response.text).features
            if x["properties"]["status"] == "OK"
        ]

    if events:
        api = (
            None
            if dry_run
            else twitter.Api(
                consumer_key=consumer_key,
                consumer_secret=consumer_secret,
                access_token_key=access_key,
                access_token_secret=access_secret,
                input_encoding="utf-8",
            )
        )
        tweet(api, until, len(events))


def tweet(api, until, events_count):
    url = f"bagage.gitlab.io/fome/?until={until}"
    tweet = f"{events_count} #courses d'#orientation à venir "
    if until == "week":
        tweet += "cette semaine"
    elif until == "month":
        tweet += "ce mois"
    else:
        tweet += "cette année"

    tweet += f" en France. Tout voir sur {url} @FFCOrientation"

    LOG.info(f"Found {events_count} events, tweeting about it now :): {tweet}")
    if api:
        api.PostUpdate(tweet)
