import csv
import datetime
import geocoder
import hashlib
import logging
import os
import requests
import sys

LOG = logging.getLogger(__name__)


class FfcoEvent(object):
    def __init__(self, name, event_date, coords, accurate, website, extra_data):
        self.name = name
        self.event_date = event_date
        self.coords = coords
        self.accurate = accurate
        self.website = website
        self.extra_data = extra_data

    def __repr__(self):
        return (
            f"{self.name}(on {self.event_date} at {self.coords}, more {self.website})"
        )


def get_position(department, address, insee, lat, lon, checksum):
    DEPTS = {
        "01": ("Ain", [46.099_798_943_085_6, 5.348_763_819_374_75]),
        "02": ("Aisne", [49.561_151_661_457_7, 3.559_302_973_902_06]),
        "03": ("Allier", [46.393_637_088_938_4, 3.187_644_008_198_35]),
        "04": ("Alpes-de-Haute-Provence", [44.106_117_124_821_9, 6.244_654_261_376_58]),
        "05": ("Hautes-Alpes", [44.663_971_945_917_8, 6.265_316_250_705_79]),
        "06": ("Alpes-Maritimes", [43.937_936_327_230_9, 7.116_531_926_656_18]),
        "07": ("Ardèche", [44.752_777_986_223_6, 4.425_583_699_679_28]),
        "08": ("Ardennes", [49.616_226_215_465_9, 4.640_750_791_099_91]),
        "09": ("Ariège", [42.921_054_077_937_8, 1.503_621_049_656_43]),
        "10": ("Aube", [48.304_634_425_946_6, 4.161_446_675_535_54]),
        "11": ("Aude", [43.103_223_806_579_1, 2.413_647_766_379_5]),
        "12": ("Aveyron", [44.281_103_349_178_4, 2.678_577_308_385_36]),
        "13": ("Bouches-du-Rhône", [43.543_054_522_443_9, 5.086_206_648_825_52]),
        "14": ("Calvados", [49.099_823_732_690_4, -0.361_579_134_527_495]),
        "15": ("Cantal", [45.051_246_548_161_5, 2.669_049_886_768_55]),
        "16": ("Charente", [45.718_715_219_069_5, 0.203_012_211_731_247]),
        "17": ("Charente-Maritime", [45.782_205_827_305, -0.675_763_824_562_911]),
        "18": ("Cher", [47.065_795_392_037_6, 2.491_142_937_556_83]),
        "19": ("Corrèze", [45.357_339_667_986_2, 1.877_776_336_577_41]),
        "21": ("Corse-du-Sud", [47.425_612_510_907_1, 4.771_662_307_007_35]),
        "22": ("Haute-Corse", [48.440_924_840_990_7, -2.864_692_574_979_15]),
        "23": ("Côte-d'Or", [46.090_620_508_335_4, 2.018_229_807_644_83]),
        "24": ("Côtes-d'Armor", [45.104_947_471_664_4, 0.741_202_931_255_545]),
        "25": ("Creuse", [47.165_963_579_824_1, 6.362_721_266_333_66]),
        "26": ("Dordogne", [44.685_229_465_030_2, 5.167_330_611_495_77]),
        "27": ("Doubs", [49.113_582_835_790_4, 0.996_540_988_903_355]),
        "28": ("Drôme", [48.388_030_652_589_4, 1.369_986_333_272_71]),
        "29": ("Eure", [48.261_900_051_069_2, -4.060_936_875_646_35]),
        "2A": ("Eure-et-Loir", [41.864_042_219_151_4, 8.987_765_326_486_38]),
        "2B": ("Finistère", [42.394_817_668_655_7, 9.206_336_167_501_05]),
        "30": ("Gard", [43.993_603_242_464_9, 4.179_851_741_350_85]),
        "31": ("Haute-Garonne", [43.358_783_302_093_7, 1.174_780_946_432_5]),
        "32": ("Gers", [43.692_940_655_25, 0.452_947_483_228_222]),
        "33": ("Gironde", [44.823_583_770_924_4, -0.575_964_836_848_079]),
        "34": ("Hérault", [43.579_416_119_384_9, 3.368_661_403_891_05]),
        "35": ("Ille-et-Vilaine", [48.155_313_986_590_6, -1.638_320_970_061_02]),
        "36": ("Indre", [46.778_243_057_069_5, 1.575_905_440_522_25]),
        "37": ("Indre-et-Loire", [47.258_572_738_923_4, 0.691_065_430_014_097]),
        "38": ("Isère", [45.264_082_231_588_5, 5.573_984_538_504_78]),
        "39": ("Jura", [46.729_367_149_579_8, 5.697_360_769_427_89]),
        "40": ("Landes", [43.965_855_156_885, -0.783_792_868_294_771]),
        "41": ("Loir-et-Cher", [47.616_880_602_317_2, 1.427_877_612_136_91]),
        "42": ("Loire", [45.728_000_596_421, 4.164_523_998_146_96]),
        "43": ("Haute-Loire", [45.128_115_419_265_3, 3.806_297_897_209_76]),
        "44": ("Loire-Atlantique", [47.363_076_743_540_7, -1.678_603_707_655_54]),
        "45": ("Loiret", [47.911_890_292_168_3, 2.343_945_396_962_49]),
        "46": ("Lot", [44.624_626_274_638, 1.605_474_497_171_37]),
        "47": ("Lot-et-Garonne", [44.367_963_693_737, 0.460_747_380_762_841]),
        "48": ("Lozère", [44.517_536_709_601_2, 3.499_703_292_094_13]),
        "49": ("Maine-et-Loire", [47.389_868_261_464_5, -0.559_432_125_666_685]),
        "50": ("Manche", [49.081_388_402_638_8, -1.329_738_367_522_57]),
        "51": ("Marne", [48.949_902_192_311_1, 4.238_455_062_589_61]),
        "52": ("Haute-Marne", [48.110_468_279_913_4, 5.225_563_658_461_03]),
        "53": ("Mayenne", [48.147_286_869_859_8, -0.657_411_947_833_804]),
        "54": ("Meurthe-et-Moselle", [48.788_798_654_668_9, 6.161_976_561_770_93]),
        "55": ("Meuse", [48.991_111_849_104_4, 5.381_349_676_916_91]),
        "56": ("Morbihan", [47.846_933_938_121_7, -2.812_483_889_670_54]),
        "57": ("Moselle", [49.037_535_811_853_4, 6.661_292_853_289_19]),
        "58": ("Nièvre", [47.115_776_740_742_1, 3.504_294_377_747_19]),
        "59": ("Nord", [50.447_568_217_32, 3.215_563_998_891_72]),
        "60": ("Oise", [49.409_938_773_075_1, 2.425_102_613_631_47]),
        "61": ("Orne", [48.623_088_238_344_2, 0.127_804_839_005_591]),
        "62": ("Pas-de-Calais", [50.493_841_008_314_1, 2.286_330_181_346_29]),
        "63": ("Puy-de-Dôme", [45.725_887_467_757_2, 3.140_162_326_012_62]),
        "64": ("Pyrénées-Atlantiques", [43.256_270_865_478_5, -0.761_274_015_439_106]),
        "65": ("Hautes-Pyrénées", [43.053_814_847_822_5, 0.163_764_301_560_443]),
        "66": ("Pyrénées-Orientales", [42.599_807_210_322_6, 2.522_288_449_809_86]),
        "67": ("Bas-Rhin", [48.671_424_954_723_1, 7.552_068_212_039_09]),
        "68": ("Haut-Rhin", [47.859_477_833_568_5, 7.273_940_432_637_74]),
        "69": ("Rhône", [45.892_311_335_797_1, 4.598_770_178_611_84]),
        "70": ("Haute-Saône", [47.641_209_113_565_8, 6.087_238_352_172_36]),
        "71": ("Saône-et-Loire", [46.644_766_711_271_8, 4.542_796_203_961_76]),
        "72": ("Sarthe", [47.994_930_196_499_1, 0.222_552_901_194_611]),
        "73": ("Savoie", [45.477_569_130_055_5, 6.442_832_588_918_55]),
        "74": ("Haute-Savoie", [46.052_700_286_060_4, 6.432_985_848_897_79]),
        "75": ("Paris", [48.856_623_645_836_2, 2.342_872_361_358_14]),
        "76": ("Seine-Maritime ", [49.654_279_137_704_5, 1.025_786_781_192_19]),
        "77": ("Seine-et-Marne", [48.627_496_857_121_9, 2.934_085_164_946_87]),
        "78": ("Yvelines", [48.815_360_168_832_9, 1.841_259_140_049_06]),
        "79": ("Deux-Sèvres", [46.556_963_480_095_1, -0.317_757_626_842_838]),
        "80": ("Somme", [49.957_796_561_87, 2.276_425_059_980_09]),
        "81": ("Tarn", [43.785_736_579_670_9, 2.165_715_709_549_88]),
        "82": ("Tarn-et-Garonne", [44.085_823_104_018_9, 1.282_306_075_684_05]),
        "83": ("Var", [43.441_655_960_643, 6.244_482_310_378_04]),
        "84": ("Vaucluse", [44.007_170_787_053_8, 5.177_326_126_528_09]),
        "85": ("Vendée", [46.674_402_756_531_1, -1.298_866_541_206_05]),
        "86": ("Vienne", [46.564_974_177_124_6, 0.459_457_215_597_168]),
        "87": ("Haute-Vienne", [45.892_294_091_948_7, 1.234_866_612_191_79]),
        "88": ("Vosges", [48.196_273_759_746_7, 6.380_139_838_964_69]),
        "89": ("Yonne", [47.840_542_887_475_2, 3.563_287_509_651_31]),
        "90": ("Territoire de Belfort", [47.631_819_332_443_1, 6.928_674_724_909_9]),
        "91": ("Essone", [48.522_615_029_102_1, 2.243_409_454_119_47]),
        "92": ("Hauts-de-Seine", [48.847_555_360_444_5, 2.246_051_877_436_99]),
        "93": ("Seine-Saint-Denis", [48.917_575_454_085_4, 2.478_539_376_433_19]),
        "94": ("Val-de-Marne", [48.777_451_083_494_8, 2.469_262_075_803_12]),
        "95": ("Val-d'Oise", [49.082_756_816_866_6, 2.130_855_586_483_42]),
        "98": ("Nouvelle-Calédonie", [-20.478, 165.146]),
    }

    dept_insee = department.zfill(2)  # force depts to be 01,02,… instead of 1,2,3…
    (dept_name, dept_pos) = DEPTS[dept_insee]

    # if the reverse geocoding found a match lat/lon, use it
    if lat and lon:
        if insee.startswith(dept_insee):
            # ensure that even if there are 2 races at the same place, they are distinct on server
            return ([float(lat) + checksum, float(lon) + checksum], True)
        else:
            log = f"address was geocoded but city {insee} does not match expected department {dept_insee},"
            log += " trying next geocoding service…"
            LOG.debug(log)

    # otherwise if an address is defined,try alternate geocoders
    if address:
        addr = f"{address}, {dept_name}"
        g = geocoder.geonames(addr, key="fome")
        if g.status == "OK":
            for response in g:
                if response.state_code.startswith(dept_insee):
                    return ([float(response.lat), float(response.lng)], True)

        if os.environ.get("GOOGLE_API_KEY"):
            g = geocoder.google(f"{addr}, France")
            if g.latlng:
                return (g.latlng, True)
        LOG.debug(
            f"could not find address {addr} for event {checksum}, using department inaccurate value instead"
        )
    else:
        LOG.debug("no address defined for this event…")

    c = dept_pos

    return ([c[0] + checksum, c[1] + checksum], False)


def fetch_ffco_calendar(file):
    """
    Fetch and return a list of events in the future.
    Event structure:
    Date;Validation;CN;Nom;Lieu;Département;Manifestation;Groupe;Spécialité;Epreuve;
    Code Organisateur;Nom Organisateur;Arbitre;Arbitre stagiaire;Contrôleur;Délégué;Contact;
    Tel;E-mail;Site web;Observations
    """
    FFCO_CSV_URL = "http://www.ffcorientation.fr/decouvrir/pratiquer/agenda/export"
    API_ADDRESSE_DATA_GOUV_URL = "https://api-adresse.data.gouv.fr/search/csv"

    LOG.info("Fetching FFCO calendar…")
    files = {}
    if file:
        content = file.read()
    else:
        ffco_response = requests.get(
            FFCO_CSV_URL, params={"du": datetime.datetime.now().strftime("%Y-%m-%d")}
        )
        if ffco_response.status_code != 200:
            LOG.critical(
                f"Invalid response from server ({ffco_response.status_code}): {ffco_response.text} "
            )
            sys.exit(1)

        content = ffco_response.content.decode("latin-1")

    lines = content.splitlines()
    eventReader = csv.reader(lines, delimiter=";")

    lines = []
    for row in eventReader:
        # cf https://github.com/etalab/adresse.data.gouv.fr/issues/631
        row[4] = row[4].replace("'", " ").replace("«", " ").replace("»", " ")
        lines.append('"' + '";"'.join(row) + '"')

    files = {"data": "\n".join(lines)}

    data = {"columns": ["Lieu", "Département"]}

    addresse_response = requests.post(
        API_ADDRESSE_DATA_GOUV_URL, data=data, files=files
    )
    if addresse_response.status_code != 200:
        LOG.critical(
            f"Invalid response from server ({addresse_response.status_code}): {addresse_response.text} "
        )
        sys.exit(1)

    decoded_addresse_csv = addresse_response.content.decode("utf-8")

    lines = decoded_addresse_csv.splitlines()
    eventReader = csv.reader(lines, delimiter=";")

    rows = [row for row in eventReader if row[0] != "Date"]
    LOG.info(f"Found {len(rows)} FFCO events.")

    return rows


def ffco_checksum(row):
    """
    Generate a unique ID based on FFCO data
    """
    hash_row = row[:1] + row[2:]  # remove status column
    md5_hash = hashlib.md5(",".join(hash_row).encode("utf-8"))
    return (
        int.from_bytes(md5_hash.digest(), "big") / (2 ** (md5_hash.digest_size * 8))
        - 0.5
    ) / 10


def prepare_ffco_event(row):
    try:
        event_date = datetime.datetime.strptime(row[0], "%d/%m/%Y").strftime("%Y-%m-%d")
        name = row[3]
        website = row[19]
        (coords, accurate) = get_position(
            row[5], row[4], row[33], row[21], row[22], ffco_checksum(row)
        )
    except Exception as e:
        LOG.error(f"could not parse CSV entry {row}: {e}, skipping")
        return None

    return FfcoEvent(name, event_date, coords, accurate, website, row)
