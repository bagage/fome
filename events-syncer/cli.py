#!/usr/bin/env python3

import json
import logging
import coloredlogs
import os
from progress.bar import IncrementalBar
import sys
import argparse
import twitter
import locale

from ffco import fetch_ffco_calendar, prepare_ffco_event
from oedb import fetch_oedb_events, prepare_oedb_event, submit_oedb_event, OedbError
from utils import tweet

LOG = logging.getLogger(__name__)


def main(
    oedb_json,
    ffco_csv,
    dry_run,
    consumer_key,
    consumer_secret,
    access_key,
    access_secret,
):
    created = []
    skipped = []
    deleted_oedb = []
    failed = []

    existing_oedb = {e.properties["checksum"]: e for e in fetch_oedb_events(oedb_json)}
    ffco_events = fetch_ffco_calendar(ffco_csv)
    if len(ffco_events):
        for row in IncrementalBar("Sync FFCO to OEDB").iter(ffco_events):
            ffco_event = prepare_ffco_event(row)

            if not ffco_event:
                continue

            oedb_event = prepare_oedb_event(ffco_event)
            if oedb_event:
                if oedb_event.properties["checksum"] in existing_oedb:
                    # already up-to-date
                    skipped.append(oedb_event)
                else:
                    try:
                        if not dry_run:
                            submit_oedb_event(oedb_event)
                        created.append(oedb_event)
                    except OedbError as e:
                        LOG.error(f"Could not create event {oedb_event}: {e}")
                        failed.append(ffco_event)
            else:
                failed.append(ffco_event)

    # do not remove events if there were some failures that might be due to server being unresponsive/unreliable
    to_remove = existing_oedb.values()
    if len(failed) == 0 and len(to_remove):
        for outdated_event in IncrementalBar("Remove OEDB outdated").iter(to_remove):
            try:
                if not dry_run:
                    submit_oedb_event(outdated_event, True)
                deleted_oedb.append(outdated_event)
            except OedbError as e:
                LOG.error(f"Could not delete event {outdated_event}", exc_info=e)
                failed.append(outdated_event)

    # tweet created events
    api = (
        None
        if dry_run
        or not consumer_key
        or not consumer_secret
        or not access_key
        or not access_secret
        else twitter.Api(
            consumer_key=consumer_key,
            consumer_secret=consumer_secret,
            access_token_key=access_key,
            access_token_secret=access_secret,
            input_encoding="utf-8",
        )
    )
    for e in created:
        tweet(dry_run, api, e)

    LOG.info(f"* Created {len(created)} events")
    LOG.debug(json.dumps(created))
    LOG.info(f"* Skipped {len(skipped)} events")
    LOG.debug(json.dumps(skipped))
    LOG.warning(f"* Failed {len(failed)} events")
    LOG.debug(json.dumps(failed, default=str))
    LOG.info(f"* Deleted {len(deleted_oedb)} events")
    LOG.debug(json.dumps(deleted_oedb))


def setup_logging(debug):
    verbosity = {
        "DEBUG": logging.DEBUG,
        "INFO": logging.INFO,
        "WARNING": logging.WARNING,
        "ERROR": logging.ERROR,
        "CRITICAL": logging.CRITICAL,
    }
    coloredlogs.install(
        level=verbosity[
            (os.environ.get("FOME_VERBOSITY") or "DEBUG" if debug else "INFO").upper()
        ],
        logger=LOG,
        fmt="%(asctime)s %(message)s",
    )


class ArgumentParser(object):
    def __init__(self):
        parser = argparse.ArgumentParser(description="insert FFCO events in OEDB")
        parser.add_argument("--debug", help="Activate debug logs", action="store_true")
        parser.add_argument(
            "--dry-run",
            "-n",
            help="Simulate actions without executing them (tweets, etc.)",
            action="store_true",
        )

        parser.add_argument("--consumer-key", help="Twitter consumer key")
        parser.add_argument("--consumer-secret", help="Twitter consumer secret")
        parser.add_argument("--access-key", help="Twitter access key")
        parser.add_argument("--access-secret", help="Twitter access secret")

        parser.add_argument(
            "--ffco-csv",
            help="File containing FFCO events, avoid requesting FFCO server",
            type=argparse.FileType("r"),
        )
        parser.add_argument(
            "--oedb-json",
            help="File containing OEDB events, avoid requesting OEDB server",
            type=argparse.FileType("r"),
        )
        self.args = parser.parse_args(sys.argv[1:])


if __name__ == "__main__":
    locale.setlocale(locale.LC_ALL, "fr_FR.utf-8")
    args = ArgumentParser().args
    setup_logging(args.debug)
    try:
        main(
            args.oedb_json,
            args.ffco_csv,
            args.dry_run,
            args.consumer_key,
            args.consumer_secret,
            args.access_key,
            args.access_secret,
        )
    except KeyboardInterrupt:
        pass
