import datetime
import logging
import requests

LOG = logging.getLogger(__name__)


def tiny_url(url):
    if url:
        apiurl = "http://tinyurl.com/api-create.php?url="
        tiny_url = requests.get(apiurl + url).content.decode("utf-8")
        return tiny_url if len(tiny_url) < len(url) else url

    return "bagage.gitlab.io/fome/"


def tweet(dry_run, api, event):
    website = event["properties"].get("source")
    when = event["properties"].get("when")
    label = event["properties"].get("label")

    url = tiny_url(website if website else "bagage.gitlab.io/fome/?until=nextyear")
    whenFmt = datetime.date.fromisoformat(when).strftime("%a %d %B")

    tweet = f"#course d'#orientation le {whenFmt} : {label}. Plus d'infos sur {url} @FFCOrientation"

    if dry_run:
        LOG.info(f"New event, simulating tweet about it now :): {tweet}")
    elif api:
        LOG.info(f"New event, tweeting about it now :): {tweet}")
        api.PostUpdate(tweet)
